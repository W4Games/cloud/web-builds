FROM docker.io/denoland/deno:2.0.3 as build-stage

WORKDIR /build

RUN apt-get update && apt-get install -y ca-certificates

# These steps will be re-run upon each file change in your working directory:
COPY . .

# Check using linter/formatter
RUN deno fmt --check

# Cache dependencies.
RUN deno cache src/main.ts

CMD ["deno", "run", "--allow-env", "--allow-read", "--allow-net", "--unstable-temporal", "src/main.ts", "--production"]

# Compile as a standalone app.
#RUN deno compile --output bin/app --allow-env --allow-read --allow-net --unstable-temporal src/main.ts --production
#
#FROM docker.io/debian:bookworm-slim AS build-release-stage
#
#WORKDIR /
#
#COPY --from=build-stage /build/bin/app /app/app
#COPY --from=build-stage /build/VERSION /app/VERSION
#COPY --from=build-stage /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
#
#CMD ["/app/app"]
