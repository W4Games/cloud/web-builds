/*************************************************************************/
/*  brotli.ts                                                            */
/*************************************************************************/
/* Copyright W4 Games Limited                                            */
/* SPDX-License-Identifier: AGPL-3.0-only                                */
/*************************************************************************/

import brotliPromise from "npm:brotli-wasm"; // Import the default export

const brotli = await brotliPromise; // Import is async in browsers due to wasm requirements!

export class SplitterStream extends TransformStream {
	constructor(output_chunk_size = 65536) {
		super({
			transform: (chunk, controller) => {
				let index = 0;
				while (index <= chunk.length) {
					const subchunk = chunk.slice(index, index + output_chunk_size);
					controller.enqueue(subchunk);
					index += output_chunk_size;
				}
			},
		});
	}
}

export class BrotliCompressionStream extends TransformStream {
	constructor(output_chunk_size = 65536) {
		const compressStream = new brotli.CompressStream();
		super({
			transform(chunk, controller) {
				let resultCode;
				let inputOffset = 0;

				// Compress this chunk, producing up to output_chunk_size output bytes at a time, until the
				// entire input has been compressed.
				do {
					const input = chunk.slice(inputOffset);
					const result = compressStream.compress(input, output_chunk_size);
					controller.enqueue(result.buf);
					resultCode = result.code;
					inputOffset += result.input_offset;
				} while (resultCode === brotli.BrotliStreamResultCode.NeedsMoreOutput);
				if (resultCode !== brotli.BrotliStreamResultCode.NeedsMoreInput) {
					controller.error(
						`Brotli compression failed when transforming with code ${resultCode}`,
					);
				}
			},
			flush(controller) {
				// Once the chunks are finished, flush any remaining data (again in repeated fixed-output
				// chunks) to finish the stream:
				let resultCode;
				do {
					const result = compressStream.compress(undefined, output_chunk_size);
					controller.enqueue(result.buf);
					resultCode = result.code;
				} while (resultCode === brotli.BrotliStreamResultCode.NeedsMoreOutput);
				if (resultCode !== brotli.BrotliStreamResultCode.ResultSuccess) {
					controller.error(
						`Brotli compression failed when flushing with code ${resultCode}`,
					);
				}
				controller.terminate();
			},
		});
	}
}

const compressChunk = function (compressStream, buffer, chunk) {
	let resultCode = 0;
	let inputOffset = 0;
	let buf = buffer;

	// Compress this chunk, producing up to output_chunk_size output bytes at a time, until the
	// entire input has been compressed.
	do {
		const input = chunk ? chunk.slice(inputOffset) : undefined;
		const result = compressStream.compress(input, 65536);
		resultCode = result.code;

		// Create new buffer
		const newBuf = new Uint8Array(buf.length + result.buf.length);
		newBuf.set(buf, 0);
		newBuf.set(result.buf, buf.length);
		buf = newBuf;

		inputOffset += result.input_offset;
	} while (resultCode === brotli.BrotliStreamResultCode.NeedsMoreOutput);

	if (
		resultCode !== brotli.BrotliStreamResultCode.NeedsMoreInput &&
		resultCode !== brotli.BrotliStreamResultCode.ResultSuccess
	) {
		throw new Error("Error when compressing to brotli: " + resultCode);
	} else {
		return buf;
	}
};

const compressChunks = function (
	compressStream,
	buffer,
	chunks,
	resolve,
	reject,
) {
	try {
		if (chunks.length) {
			const chunk = chunks[0];
			chunks.shift();
			const buf = compressChunk(compressStream, buffer, chunk);
			setTimeout(
				compressChunks.bind(null, compressStream, buf, chunks, resolve, reject),
				0,
			);
		} else {
			const buf = compressChunk(compressStream, buffer, undefined);
			resolve(buf);
		}
	} catch (e) {
		reject(e);
	}
};

export async function brotliCompress(stream, chunk_size = 524288) {
	const splitted = stream.pipeThrough(new SplitterStream(chunk_size));
	const chunks = [];
	for await (const chunk of splitted) {
		chunks.push(chunk);
	}
	const buffer = new Uint8Array();
	const compressStream = new brotli.CompressStream();
	return new Promise((resolve, reject) => {
		setTimeout(
			compressChunks.bind(
				null,
				compressStream,
				buffer,
				chunks,
				resolve,
				reject,
			),
			0,
		);
	});
}
