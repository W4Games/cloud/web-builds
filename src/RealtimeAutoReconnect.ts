/*************************************************************************/
/*  RealtimeAutoReconnect.ts                                             */
/*************************************************************************/
/* Copyright W4 Games Limited                                            */
/* SPDX-License-Identifier: AGPL-3.0-only                                */
/*************************************************************************/

// deno-lint-ignore-file no-explicit-any

import { RealtimeChannel, SupabaseClient } from "jsr:@supabase/supabase-js";
import { delay } from "jsr:@std/async@^0.224.2";

const DURATION_FOR_VALID_CONNECTION_SECONDS = 10;
const MIN_DELAY_MILLISECONDS = 500;
const MAX_DELAY_MILLISECONDS = 10000;

export default class RealtimeAutoReconnect<SchemaName extends string> {
	_onConnectedCallbacks: Array<() => void> = [];
	_onDisconnectedCallbacks: Array<() => void> = [];

	_supabaseClient: SupabaseClient<any, SchemaName, any>;
	_channelName: string;
	_channelCreatedCallback: (channel: RealtimeChannel) => RealtimeChannel;

	_debug: boolean = true;

	_channel: RealtimeChannel | undefined;
	_shouldStop: boolean = false;
	_consecutiveConnectionErrors: number = 0;
	_delay: number = MIN_DELAY_MILLISECONDS;

	_connected = false;
	_lastConnectionTime = new Temporal.Instant(0n);

	_mainLoopPromise: Promise<void> | undefined;

	constructor(
		supabaseClient: SupabaseClient<any, SchemaName, any>,
		channelName: string,
		channelCreatedCallback: (channel: RealtimeChannel) => RealtimeChannel = (
			channel: RealtimeChannel,
		) => channel,
	) {
		this._supabaseClient = supabaseClient;
		this._channelName = channelName;
		this._channelCreatedCallback = channelCreatedCallback;
	}

	onConnected(callback: () => void): RealtimeAutoReconnect<SchemaName> {
		this._onConnectedCallbacks.push(callback);
		return this;
	}

	onDisconnected(callback: () => void): RealtimeAutoReconnect<SchemaName> {
		this._onDisconnectedCallbacks.push(callback);
		return this;
	}

	run() {
		this._mainLoopPromise = this._run();
	}

	async stop() {
		this._shouldStop = true;
		await this._mainLoopPromise;
	}

	isConnected() {
		return this._channel && this._channel.state == "joined";
	}

	async _run() {
		this._shouldStop = false;
		while (!this._shouldStop) {
			if (!this._channel) {
				await this._reconnect();
			} else if (this._channel.state == "joined") {
				if (!this._connected) {
					this._connected = true;
					this._lastConnectionTime = Temporal.Now.instant();
				}
			} else if (
				this._channel.state == "closed" || this._channel.state == "errored"
			) {
				if (this._connected) {
					if (
						(Temporal.Now.instant().epochMilliseconds -
							this._lastConnectionTime.epochMilliseconds) >
							DURATION_FOR_VALID_CONNECTION_SECONDS * 1000
					) {
						// If we were connected for at least 10 seconds, we reset the "consecutive" counter.
						this._consecutiveConnectionErrors = 0;
						this._delay = MIN_DELAY_MILLISECONDS;
					}
					this._connected = false;
				}

				if (this._consecutiveConnectionErrors > 2) {
					this._delay = Math.min(this._delay * 2, MAX_DELAY_MILLISECONDS);
					this._logMessage(
						`Connection failed after ${this._consecutiveConnectionErrors} consecutive errors. Trying again in ${
							this._delay / 1000
						} seconds...`,
					);
					await delay(this._delay);
				} else {
					this._logMessage(
						`Connection failed after ${this._consecutiveConnectionErrors} consecutive errors. Trying again.`,
					);
				}
				await this._reconnect();
				this._consecutiveConnectionErrors += 1;
			}
			await delay(100);
		}
	}

	async _reconnect() {
		if (this._channel) {
			this._logMessage("Unsubscribing to channel...");
			const res = await this._supabaseClient.removeChannel(this._channel);
			if (res !== "ok") {
				this._logError("Could not unsubscribe successfully: ", res);
				return;
			}
		}
		this._logMessage(
			`Subscribing to \"${this._channelName}\" realtime channel ...`,
		);
		this._channel = this._supabaseClient.channel(this._channelName);
		this._channel = this._channelCreatedCallback(
			this._channel as RealtimeChannel,
		);
		if (this._channel.joinedOnce) {
			this._channel = undefined;
			throw new Error(
				"The _channelCreatedCallback callback provided to RealtimeAutoReconnect should return a never-connected RealtimeChannel.",
			);
		}
		this._channel.subscribe((event, info) => {
			if (event === "SUBSCRIBED") {
				this._logMessage("Subscribed successfully.");
				this._lastConnectionTime = Temporal.Now.instant();
				this._onConnectedCallbacks.forEach((c) => c());
			} else if (event === "CHANNEL_ERROR") {
				this._logError("Subscription error: ", info);
				this._onDisconnectedCallbacks.forEach((c) => c());
			} else if (event === "CLOSED" && !this._shouldStop) {
				this._logError("Connection unexpectedly closed.");
				this._onDisconnectedCallbacks.forEach((c) => c());
			}
		});
	}

	_logMessage(...message: any[]) {
		if (!this._debug) {
			return;
		}
		console.log(...message);
	}

	_logError(...message: any[]) {
		if (!this._debug) {
			return;
		}
		console.error(...message);
	}
}
