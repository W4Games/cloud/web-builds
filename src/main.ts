/*************************************************************************/
/*  main.ts                                                              */
/*************************************************************************/
/* Copyright W4 Games Limited                                            */
/* SPDX-License-Identifier: AGPL-3.0-only                                */
/*************************************************************************/

import {
	BlobReader,
	BlobWriter,
	configure,
	ZipReader,
} from "jsr:@zip-js/zip-js";
import { load } from "jsr:@std/dotenv";
import { delay } from "jsr:@std/async";
import * as path from "jsr:@std/path";
import { createClient } from "jsr:@supabase/supabase-js";
import { parseArgs } from "jsr:@std/cli/parse-args";
import { mime } from "https://deno.land/x/mimetypes@v1.0.0/mod.ts";

import RealtimeAutoReconnect from "./RealtimeAutoReconnect.ts";

import { brotliCompress } from "./brotli.ts";

const COMPRESSED_EXTENSIONS = "wasm,pck";

// Import .env files.
const flags = parseArgs(Deno.args, {
	boolean: ["development", "production"],
});
if (flags.development == flags.production) {
	console.error("Should be started with either --development or --production");
	Deno.exit(1);
}

const envName = flags.development ? "development" : "production";
await load({ envPath: `.env.${envName}.local`, export: true });
await load({ envPath: `.env.${envName}`, export: true });

const supabaseUrl = Deno.env.get("SUPABASE_URL") ?? "";
const supabaseKey = Deno.env.get("SUPABASE_SERVICE_KEY") ?? "";
const webBuildCompressedBucket = Deno.env.get("BUCKET_COMPRESSED_BUILDS") ?? "";
const webBuildUnCompressedPublicBucket =
	Deno.env.get("BUCKET_PUBLIC_WEB_GAMES") ?? "";
const webBuildUnCompressedPrivateBucket =
	Deno.env.get("BUCKET_PRIVATE_WEB_GAMES") ?? "";

// Deno configuration (Web Worker doesn't support blobs when compiled).
configure({
	useWebWorkers: false,
});

// Types
export type WebBuild = {
	name: string;
	slug: string;
	object_id: string;
	object_name: string;
	public: boolean;
	state: string;
	message: string;
};

// Deletes a folder in the given bucket.
async function deleteFolder(bucket: string, folder: string) {
	// Delete the folders in public bucket, if it exists.
	const resListFolder = await supabase.storage.from(bucket).list(folder, {
		limit: 100000,
	});
	if (resListFolder.error) {
		throw new Error(
			"Could not list files in folder to delete: " + resListFolder.error,
		);
	}

	// Delete subfolders
	const foldersToRemove = resListFolder.data.filter((f) => (!(f.id)));
	for (const i in foldersToRemove) {
		await deleteFolder(bucket, `${folder}/${foldersToRemove[i].name}`);
	}

	// Delete files
	const filesToRemove = resListFolder.data.filter((f) => (f.id));
	if (filesToRemove.length > 0) {
		const filesToRemoveNames = filesToRemove.map((f) => `${folder}/${f.name}`);
		const chunkSize = 20;
		for (let i = 0; i < filesToRemoveNames.length; i += chunkSize) {
			const chunk = filesToRemoveNames.slice(i, i + chunkSize);
			const resDeleteFolder = await supabase.storage.from(bucket).remove(chunk);
			if (resDeleteFolder.error) {
				throw new Error("Could not delete files: " + resDeleteFolder.error);
			}
		}
	}
}

// Create supabase client.
const supabase = createClient(supabaseUrl, supabaseKey, {
	db: { schema: "w4online" },
});

// Check if the buckets exist, and if not, create them.
console.log("Fetching bucket list and create buckets if needed...");
const resListBuckets = await supabase.storage.listBuckets();
if (resListBuckets.error) {
	throw new Error(resListBuckets.error.message);
}

const bucketList = resListBuckets.data.map((b) => (b.name));
if (!bucketList.includes(webBuildUnCompressedPublicBucket)) {
	const resCreateBucket = await supabase.storage.createBucket(
		webBuildUnCompressedPublicBucket,
		{ public: true },
	);
	if (resCreateBucket.error) {
		throw new Error(resCreateBucket.error.message);
	}
}
if (!bucketList.includes(webBuildUnCompressedPrivateBucket)) {
	const resCreateBucket = await supabase.storage.createBucket(
		webBuildUnCompressedPrivateBucket,
		{ public: false },
	);
	if (resCreateBucket.error) {
		throw new Error(resCreateBucket.error.message);
	}
}

// Dirty status.
let isDirty = false;

// To check if everything is healthy
const ERROR_HEALTH_THRESHOLD = 10;
let errorCount = 0;
function isHealthy() {
	return errorCount < ERROR_HEALTH_THRESHOLD &&
		realtimeAutoReconnect.isConnected();
}

// Start and health check server.
const healthServer = Deno.serve({ port: 3000 }, (req) => {
	if (new URL(req.url).pathname === "/health") {
		if (isHealthy()) {
			return new Response("Healthy\n", { status: 200 });
		} else {
			return new Response("Service Unavailable\n", { status: 503 });
		}
	} else {
		return new Response("Not Found\n", { status: 404 });
	}
});

// Subscribe to database changes.
const realtimeAutoReconnect = new RealtimeAutoReconnect(
	supabase,
	"webbuilds",
	(channel) => {
		return channel.on("postgres_changes", {
			event: "*",
			schema: "w4online",
			table: "web_build",
		}, () => {
			isDirty = true;
		});
	},
);
realtimeAutoReconnect.onConnected(() => {
	isDirty = true;
});
realtimeAutoReconnect.run();

// Should shut down.
let shouldShutDown = false;
async function shutdown() {
	console.log("Shutting down");
	shouldShutDown = true;
	await realtimeAutoReconnect.stop();
	await healthServer.shutdown();
	await supabase.removeAllChannels();
	Deno.exit(0);
}
Deno.addSignalListener("SIGINT", shutdown);
Deno.addSignalListener("SIGTERM", shutdown);

// Main loop.
let numProcessed = 0;
console.log("Waiting for web builds changes...");
while (!shouldShutDown) {
	if (isDirty && realtimeAutoReconnect.isConnected()) {
		isDirty = false;
		numProcessed = 0;
		console.log("Check if builds need processing...");
		try {
			// Fetch the list of web builds.
			const resListWebBuilds = await supabase.rpc("web_builds_get_all");
			if (resListWebBuilds.error) {
				throw new Error(resListWebBuilds.error.message);
			}
			const webBuilds = resListWebBuilds.data as WebBuild[];

			// Fetch the list of web builds waiting for an update.
			const webBuildsToUpdate = webBuilds.filter((
				webBuild: WebBuild,
			) => (webBuild?.state === "needs_update"));

			// Fetch the list of folders that need to be deleted (web builds do not exist anymore).
			const webBuildSlugs = webBuilds.map((e) => (e.slug));
			const resListFolderPublic = await supabase.storage.from(
				webBuildUnCompressedPublicBucket,
			).list("", { limit: 100000 });
			if (resListFolderPublic.error) {
				throw new Error(
					"Could not list files in bucket: " + resListFolderPublic.error,
				);
			}
			const foldersPublic = resListFolderPublic.data;
			const foldersToDeletePublic = foldersPublic.filter((
				e,
			) => (!webBuildSlugs.includes(e.name))).map((e) => (e.name));
			if (foldersToDeletePublic.length > 0) {
				console.log(
					`Deleting the following folders in ${webBuildUnCompressedPublicBucket} bucket: ${
						foldersToDeletePublic.join(", ")
					}`,
				);
				for (const i in foldersToDeletePublic) {
					await deleteFolder(
						webBuildUnCompressedPublicBucket,
						foldersToDeletePublic[i],
					);
				}
			}

			const resListFolderPrivate = await supabase.storage.from(
				webBuildUnCompressedPrivateBucket,
			).list("", { limit: 100000 });
			if (resListFolderPrivate.error) {
				throw new Error(
					"Could not list files in bucket: " + resListFolderPrivate.error,
				);
			}
			const foldersPrivate = resListFolderPrivate.data;
			const foldersToDeletePrivate = foldersPrivate.filter((
				e,
			) => (!webBuildSlugs.includes(e.name))).map((e) => (e.name));
			if (foldersToDeletePrivate.length > 0) {
				console.log(
					`Deleting the following folders in ${webBuildUnCompressedPrivateBucket} bucket: ${
						foldersToDeletePrivate.join(", ")
					}`,
				);
				for (const i in foldersToDeletePrivate) {
					await deleteFolder(
						webBuildUnCompressedPrivateBucket,
						foldersToDeletePrivate[i],
					);
				}
			}

			// Function to process a given build.
			const processBuild = async (webBuild: WebBuild) => {
				// Update the status of the web build to "being processed".
				const resUpdate = await supabase.rpc("web_build_state_update", {
					slug: webBuild.slug,
					state: "update_in_progress",
					message: null,
				});
				if (resUpdate.error) {
					throw new Error("Could update web build state: " + resUpdate.error);
				}

				// Download the compressed file.
				const resDownload = await supabase.storage.from(
					webBuildCompressedBucket,
				).download(webBuild.object_name);
				if (resDownload.error) {
					throw new Error("Could not download web build: " + resDownload.error);
				}

				// Unzip
				const zipFileReader = new BlobReader(resDownload.data as Blob);
				const zipReader = new ZipReader(zipFileReader);
				const entries = await zipReader.getEntries();

				// First, check if we have a valid web build.
				if (entries.filter((e) => (e.filename === "index.html")).length == 0) {
					throw new Error("Invalid web build: no index.html file found.");
				}

				// Delete the folder in public bucket, if it exists.
				if (
					foldersPublic.filter((f) => (f.name === webBuild.slug)).length > 0
				) {
					await deleteFolder(webBuildUnCompressedPublicBucket, webBuild.slug);
				}
				if (
					foldersPrivate.filter((f) => (f.name === webBuild.slug)).length > 0
				) {
					await deleteFolder(webBuildUnCompressedPrivateBucket, webBuild.slug);
				}

				// Delete the folder in private bucket, if it exists.
				const filesToRemovePrivate = resListFolderPrivate.data.map((f) =>
					`${webBuild.slug}/${f.name}`
				);
				if (filesToRemovePrivate.length > 0) {
					const resDeleteFolder = await supabase.storage.from(
						webBuildUnCompressedPrivateBucket,
					).remove(filesToRemovePrivate);
					if (resDeleteFolder.error) {
						throw new Error(
							"Could not delete folder: " + resDeleteFolder.error,
						);
					}
				}

				// Get the bucket to use.
				const bucket = webBuild.public
					? webBuildUnCompressedPublicBucket
					: webBuildUnCompressedPrivateBucket;

				// Extract entries.
				for (const entry of entries) {
					if (!entry.directory && entry.getData) {
						// Get the file entry as a blob.
						const blobWriter = new BlobWriter();
						const blob = await entry.getData(blobWriter);

						// Upload the file to the bucket.
						const { error } = await supabase.storage.from(bucket).upload(
							path.join(webBuild.slug, entry.filename),
							await blob.arrayBuffer(),
							{
								upsert: true,
								contentType: mime.getType(entry.filename),
							},
						);
						if (error) {
							throw new Error(error.message);
						}

						// Compress the file if needed.
						const shouldBeCompressed = COMPRESSED_EXTENSIONS.split(",").some(
							(e) => {
								return entry.filename.endsWith(`.${e}`);
							},
						);
						if (shouldBeCompressed) {
							{
								// GZip compress
								const uploadedFilename = `${entry.filename}.gz`;
								console.log("Compressing: ", uploadedFilename);
								const { error } = await supabase.storage.from(bucket).upload(
									path.join(webBuild.slug, uploadedFilename),
									blob.stream().pipeThrough(new CompressionStream("gzip")),
									{
										upsert: true,
										contentType: mime.getType(uploadedFilename),
									},
								);
								if (error) {
									throw new Error(error.message);
								}
							}
							{
								// Brotli compress
								const uploadedFilename = `${entry.filename}.br`;
								console.log("Compressing: ", uploadedFilename);
								const compressed = await brotliCompress(blob.stream());
								const { error } = await supabase.storage.from(bucket).upload(
									path.join(webBuild.slug, uploadedFilename),
									compressed,
									{
										upsert: true,
										contentType: mime.getType(uploadedFilename),
									},
								);
								if (error) {
									console.error(error);
									throw new Error(error.message);
								}
							}
						}
					}
				}
			};

			// Process all builds.
			const results = await Promise.all(
				webBuildsToUpdate.map(async (webBuild: WebBuild) => {
					try {
						await processBuild(webBuild);
						return {
							slug: webBuild.slug,
							error: null,
						};
					} catch (error) {
						errorCount += 1;
						console.error(
							`An error happened while processing build ${webBuild.slug}: `,
							error,
						);
						return {
							slug: webBuild.slug,
							error: error,
						};
					}
				}),
			);

			// Update the database with the results.
			await Promise.all(results.map(async (result) => {
				const state = result.error ? "error" : "ready";
				const message = (result.error as Error)?.message || null;
				const { error } = await supabase.rpc("web_build_state_update", {
					slug: result.slug,
					state,
					message,
				});
				if (error) {
					console.log("Unexpected error: " + JSON.stringify(error));
				}
				console.log(
					result.error
						? `Error processing web build \"${result.slug}\": ${result.error}`
						: `Web build \"${result.slug}\" processed successfully`,
				);
			}));
			numProcessed = results.length;
		} catch (e) {
			console.error((e as Error).message);
			errorCount += 1;
		}
		console.log(`Processed ${numProcessed} web builds.`);
	} else {
		await delay(100);
	}
}
