# Webpump

Webpump is our microservice that unzip web game builds and make them public. It fetches the list of web builds that needs to be processed, and for each it can do multiple operations:
- unzip a web build file, then upload those unzipped files to either a public or a private bucket.
- delete files associated to a deleted build.
- move files from the public bucket to the private one (or the other way around), depending on the "public" attribute of the web build.